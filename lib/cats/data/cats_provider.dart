import 'dart:convert';

import 'package:http/http.dart' as http;
import 'package:itworld_interview/shared/provider_errors.dart';

class CatsProvider {
  final String token;
  CatsProvider({required this.token});

  final String baseUrl = 'http://mobile-test.itfox-web.com:80';

  Future<List<dynamic>> fetch(int page) async {
    final uri = Uri.parse('$baseUrl/private/list?page=$page');
    final http.Response response = await http.get(uri, headers: {
      'Authorization': 'Bearer $token',
      'content-type': 'application/json; charset=utf-8',
    });
    if (response.statusCode == 200) {
      return jsonDecode(utf8.decode(response.bodyBytes));
    }
    throw UnexpectedResponce();
  }
}
