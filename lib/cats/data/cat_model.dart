enum CatType { SMALL_CARD, BIG_CARD }
final Map<String, CatType> catTypeMap = {
  'SMALL_CARD': CatType.SMALL_CARD,
  'BIG_CARD': CatType.BIG_CARD,
};

class Cat {
  final int id;
  final String name;
  final int age;
  final String imageUrl;
  final CatType type;

  Cat({
    required this.id,
    required this.name,
    required this.age,
    required this.imageUrl,
    required this.type,
  });

  static Cat fromJson(json) => Cat(
        id: json['id'],
        name: json['name'],
        age: json['age'],
        imageUrl: json['imageUrl'],
        type: catTypeMap[json['type']] ?? CatType.SMALL_CARD,
      );

  @override
  String toString() =>
      'Cat(id: $id; name: $name; age: $age; img: $imageUrl; type: $type)';
}
