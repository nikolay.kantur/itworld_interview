import 'package:itworld_interview/cats/data/cat_model.dart';
import 'package:itworld_interview/cats/data/cats_provider.dart';

class CatsRepository {
  final CatsProvider catsProvider;
  CatsRepository({required this.catsProvider});

  Future<dynamic> fetch({
    required int page,
  }) async {
    final List<dynamic> respRaw = await catsProvider.fetch(page);
    return respRaw.map((e) => Cat.fromJson(e)).toList();
  }
}
