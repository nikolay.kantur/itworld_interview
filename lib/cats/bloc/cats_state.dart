import 'package:equatable/equatable.dart';
import 'package:itworld_interview/cats/data/cat_model.dart';

enum CatsStatus { initial, fetching, success, error }

class CatsState extends Equatable {
  final List<Cat> cats;
  final CatsStatus status;
  final int nextPage;
  final String error;

  CatsState({
    required this.cats,
    required this.status,
    required this.nextPage,
    required this.error,
  });

  CatsState copyWith({
    List<Cat>? cats,
    CatsStatus? status,
    int? nextPage,
    String? error,
  }) =>
      CatsState(
        cats: cats ?? this.cats,
        status: status ?? this.status,
        nextPage: nextPage ?? this.nextPage,
        error: error ?? this.error,
      );

  @override
  List<Object?> get props => [cats, status, nextPage];
}
