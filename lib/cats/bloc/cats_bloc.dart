import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:itworld_interview/cats/bloc/cats_event.dart';
import 'package:itworld_interview/cats/bloc/cats_state.dart';
import 'package:itworld_interview/cats/data/cat_model.dart';
import 'package:itworld_interview/cats/data/cats_repository.dart';

class CatsBloc extends Bloc<CatsEvent, CatsState> {
  final CatsRepository catsRepository;
  CatsBloc({required this.catsRepository})
      : super(CatsState(
          status: CatsStatus.initial,
          cats: [],
          nextPage: 0,
          error: '',
        ));

  @override
  Stream<CatsState> mapEventToState(event) async* {
    try {
      switch (event.runtimeType) {
        case CatsFetchEvent:
          yield* _fetch();
          break;
        default:
      }
    } catch (e) {
      yield state.copyWith(status: CatsStatus.error, error: e.toString());
    }
  }

  Stream<CatsState> _fetch() async* {
    yield state.copyWith(status: CatsStatus.fetching);
    final List<Cat> cats = await catsRepository.fetch(page: state.nextPage);
    yield state.copyWith(
      status: CatsStatus.success,
      nextPage: state.nextPage + 1,
      cats: List.of(state.cats)..addAll(cats),
    );
  }

  @override
  String toString() => 'CatsBloc';
}
