import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:itworld_interview/cats/bloc/cats_bloc.dart';
import 'package:itworld_interview/cats/bloc/cats_event.dart';
import 'package:itworld_interview/cats/bloc/cats_state.dart';
import 'package:itworld_interview/cats/data/cat_model.dart';
import 'package:itworld_interview/cats/data/cats_provider.dart';
import 'package:itworld_interview/cats/data/cats_repository.dart';
import 'package:itworld_interview/shared/auth/auth_cubit.dart';
import 'package:flutter_staggered_grid_view/flutter_staggered_grid_view.dart';

class CatsPage extends StatelessWidget {
  const CatsPage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (BuildContext context) => CatsBloc(
        catsRepository: CatsRepository(
          catsProvider:
              CatsProvider(token: context.read<AuthCubit>().state.token),
        ),
      )..add(CatsFetchEvent()),
      child: _CatsPage(),
    );
  }
}

class _CatsPage extends StatefulWidget {
  _CatsPage({Key? key}) : super(key: key);

  // static Route route() {
  //   return MaterialPageRoute<void>(builder: (_) => _CatsPage());
  // }

  @override
  _CatsPageState createState() => _CatsPageState();
}

class _CatsPageState extends State<_CatsPage> {
  int? selectedIndex;
  late ScrollController _scrollController;
  late CatsBloc _catsBloc;

  @override
  void initState() {
    super.initState();
    _scrollController = new ScrollController(initialScrollOffset: 5.0)
      ..addListener(_scrollListener);

    _catsBloc = context.read<CatsBloc>();
  }

  @override
  Widget build(BuildContext context) {
    print('here');
    return Scaffold(
      body: BlocBuilder<CatsBloc, CatsState>(
          builder: (context, state) => Padding(
                padding: EdgeInsets.all(15),
                child: StaggeredGridView.countBuilder(
                  crossAxisSpacing: 15,
                  mainAxisSpacing: 15,
                  crossAxisCount: 2,
                  itemCount: state.cats.length,
                  staggeredTileBuilder: (int index) => StaggeredTile.count(
                    state.cats[index].type == CatType.BIG_CARD ? 2 : 1,
                    1,
                  ),
                  itemBuilder: (context, index) {
                    return CatWidget(
                        cat: state.cats[index], selectedIndex: selectedIndex);
                  },
                  controller: _scrollController,
                  scrollDirection: Axis.vertical,
                ),
              )),
    );
  }

  _scrollListener() {
    if (_scrollController.offset >=
            _scrollController.position.maxScrollExtent &&
        !_scrollController.position.outOfRange) {
      _catsBloc.add(CatsFetchEvent());
    }
  }

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }
}

class CatWidget extends StatelessWidget {
  final Cat cat;
  final int? selectedIndex;
  const CatWidget({
    Key? key,
    required this.cat,
    required this.selectedIndex,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.all(Radius.circular(30)),
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.25),
              blurRadius: 4,
              offset: Offset(0, 4),
            ),
          ],
        ),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(30),
          child: Image.network(
            cat.imageUrl,
            fit: BoxFit.cover,
            loadingBuilder: (context, child, progress) => progress == null
                ? child
                : Center(
                    child: SizedBox(
                      height: 20,
                      width: 20,
                      child: CircularProgressIndicator(),
                    ),
                  ),
            errorBuilder: (BuildContext context, Object exception,
                StackTrace? stackTrace) {
              return const Text('😢');
            },
          ),
        ));
  }
}
