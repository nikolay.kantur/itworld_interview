import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:itworld_interview/login/presentation/login_page.dart';
import 'package:itworld_interview/shared/app_bloc_observer.dart';
import 'package:itworld_interview/shared/auth/auth_cubit.dart';

void main() {
  Bloc.observer = AppBlockObserver();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: BlocProvider(
        create: (_) => AuthCubit(),
        child: LoginPage(),
      ),
    );
  }
}
