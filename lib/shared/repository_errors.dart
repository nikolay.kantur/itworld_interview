abstract class RepostiryError {}

class Unauthorized extends RepostiryError {
  @override
  String toString() => 'Не авторизован';
}
