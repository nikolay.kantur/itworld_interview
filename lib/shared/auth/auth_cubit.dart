import 'package:bloc/bloc.dart';
import 'package:itworld_interview/shared/auth/auth_state.dart';

class AuthCubit extends Cubit<AuthState> {
  AuthCubit()
      : super(AuthState(
          status: AuthStatus.unknown,
          token: '',
        ));

  login(String token) {
    emit(state.copyWith(
      status: AuthStatus.authenticated,
      token: token,
    ));
  }

  logout() {
    emit(state.copyWith(status: AuthStatus.unauthenticated, token: ''));
  }

  @override
  void onChange(Change<AuthState> change) {
    super.onChange(change);
  }
}
