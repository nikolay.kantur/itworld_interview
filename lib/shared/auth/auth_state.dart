import 'package:equatable/equatable.dart';

enum AuthStatus { unknown, authenticated, unauthenticated }

class AuthState extends Equatable {
  final AuthStatus status;
  final String token;

  AuthState({
    required this.status,
    required this.token,
  });

  AuthState copyWith({
    AuthStatus? status,
    String? token,
  }) =>
      AuthState(
        status: status ?? this.status,
        token: token ?? this.token,
      );

  @override
  List<Object> get props => [status, token];
}
