abstract class ProviderError {}

class UnexpectedResponce extends ProviderError {
  @override
  String toString() {
    return 'Неожиданная ошибка';
  }
}

class WrongCredentials extends ProviderError {
  @override
  String toString() {
    return 'Неверный логин или пароль';
  }
}
