abstract class LoginFormEvent {}

class LoginChangedEvent extends LoginFormEvent {
  final String login;
  LoginChangedEvent(this.login);
}

class PasswordChangedEvent extends LoginFormEvent {
  final String password;
  PasswordChangedEvent(this.password);
}

class LoginFormSubmitEvent extends LoginFormEvent {}
