import 'dart:io';

import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:itworld_interview/login/bloc/login_event.dart';
import 'package:itworld_interview/login/bloc/login_state.dart';
import 'package:itworld_interview/login/data/login_repository.dart';
import 'package:itworld_interview/shared/auth/auth_cubit.dart';

class LoginBloc extends Bloc<LoginFormEvent, LoginState> {
  final LoginRepository loginRepository;
  final AuthCubit authCubit;

  LoginBloc({required this.loginRepository, required this.authCubit})
      : super(LoginState(
          login: 'test123',
          password: 'qwer1234',
          status: LoginStatus.initial,
        ));

  @override
  Stream<LoginState> mapEventToState(LoginFormEvent event) async* {
    switch (event.runtimeType) {
      case LoginChangedEvent:
        event as LoginChangedEvent;
        yield state.copyWith(status: LoginStatus.changed, login: event.login);
        break;
      case PasswordChangedEvent:
        event as PasswordChangedEvent;
        yield state.copyWith(
            status: LoginStatus.changed, password: event.password);
        break;
      case LoginFormSubmitEvent:
        yield* _signIn();
        break;
      default:
        throw UnimplementedError();
    }
  }

  Stream<LoginState> _signIn() async* {
    yield state.copyWith(status: LoginStatus.authentication);
    try {
      final token = await loginRepository.signIn(
          login: state.login, password: state.password);
      yield state.copyWith(status: LoginStatus.success);
      authCubit.login(token);
    } on SocketException {
      yield state.copyWith(
          status: LoginStatus.error, error: 'Ошибка интернет соединения');
    } catch (e) {
      yield state.copyWith(status: LoginStatus.error, error: e.toString());
    }
  }

  @override
  String toString() {
    return 'LoginBloc state: $state';
  }
}
