import 'package:equatable/equatable.dart';

enum LoginStatus { initial, changed, authentication, success, error }

class LoginState extends Equatable {
  final String login;
  final String password;
  final LoginStatus status;

  final String error;

  String? get loginValidator =>
      login.length < 5 ? 'Логин слишком короткий' : null;

  String? get passwordValidator =>
      password.length < 6 ? 'Пароль слишком короткий' : null;

  LoginState({
    required this.login,
    required this.password,
    required this.status,
    this.error = '',
  });

  @override
  List<Object?> get props => [login, password, status, error];

  LoginState copyWith({
    String? login,
    String? password,
    LoginStatus? status,
    String? error,
  }) =>
      LoginState(
        login: login ?? this.login,
        password: password ?? this.password,
        status: status ?? this.status,
        error: error ?? this.error,
      );
}
