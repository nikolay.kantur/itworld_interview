import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:itworld_interview/cats/presentation/cats_page.dart';
import 'package:itworld_interview/login/bloc/login_bloc.dart';
import 'package:itworld_interview/login/bloc/login_event.dart';
import 'package:itworld_interview/login/bloc/login_state.dart';
import 'package:itworld_interview/login/data/login_provider.dart';
import 'package:itworld_interview/login/data/login_repository.dart';
import 'package:itworld_interview/shared/app_colors.dart';
import 'package:itworld_interview/shared/auth/auth_cubit.dart';

class LoginPage extends StatelessWidget {
  LoginPage({Key? key}) : super(key: key);
  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  static Route route() {
    return MaterialPageRoute<void>(builder: (_) => LoginPage());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
        alignment: Alignment.center,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Container(
              width: 276,
              child: BlocProvider<LoginBloc>(
                create: (context) => LoginBloc(
                    authCubit: context.read<AuthCubit>(),
                    loginRepository:
                        LoginRepository(loginProvider: LoginProvider())),
                child: BlocConsumer<LoginBloc, LoginState>(
                  listener: (context, state) {
                    switch (state.status) {
                      case LoginStatus.success:
                        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                          content: Text('Вход выполнен'),
                          backgroundColor: AppColors.green,
                        ));

                        Navigator.of(context).pushReplacement(
                          MaterialPageRoute(
                            builder: (_) => RepositoryProvider.value(
                                value: context.read<AuthCubit>(),
                                child: CatsPage(key: UniqueKey())),
                          ),
                        );
                        break;
                      case LoginStatus.error:
                        ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                          content: Text(state.error),
                          backgroundColor: Colors.red,
                        ));
                        break;
                      default:
                    }
                  },
                  builder: (context, state) => Form(
                      key: _formKey,
                      child: Column(
                        children: [
                          _shadowedTextFormField(
                            validator: (_) => state.loginValidator,
                            onChanged: (value) => context
                                .read<LoginBloc>()
                                .add(LoginChangedEvent(value)),
                            hintText: 'Login',
                            initialValue: state.login,
                          ),
                          // SizedBox(height: 20),
                          _shadowedTextFormField(
                            validator: (_) => state.passwordValidator,
                            onChanged: (value) => context
                                .read<LoginBloc>()
                                .add(PasswordChangedEvent(value)),
                            hintText: 'Password',
                            initialValue: state.password,
                          ),
                          SizedBox(height: 20), // 40
                          _signInButton(context, state),
                        ],
                      )),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  final outlineInputBorder = const OutlineInputBorder(
    borderRadius: BorderRadius.all(Radius.circular(8)),
    borderSide: BorderSide(
      color: Color(0xFFE8E8E8),
      width: 1,
      style: BorderStyle.solid,
    ),
  );

  Widget _shadowedTextFormField({
    required String hintText,
    required String? Function(String?) validator,
    required void Function(String)? onChanged,
    String initialValue = '',
  }) =>
      Container(
          height: 70,
          child: TextFormField(
            decoration: InputDecoration(
              helperText: ' ',
              hintText: hintText,
              hintStyle: TextStyle(
                color: Color(0xFFBDBDBD),
                // font-family: Inter;
                fontWeight: FontWeight.w500,
                fontStyle: FontStyle.normal,
                fontSize: 16,
                // height: 19 / 16, // line height 19
              ),
              contentPadding:
                  EdgeInsets.symmetric(vertical: 11, horizontal: 16),
              fillColor: Color(0xFFF6F6F6),
              filled: true,
              enabledBorder: outlineInputBorder,
              focusedBorder: outlineInputBorder,
            ),
            validator: validator,
            onChanged: onChanged,
            initialValue: initialValue,
          ));

  Widget _signInButton(
    BuildContext context,
    LoginState state,
  ) =>
      SizedBox(
        width: double.infinity,
        height: 50,
        child: ElevatedButton(
          onPressed: () {
            if (state.status == LoginStatus.authentication) return;
            if (state.status == LoginStatus.error) return;
            if (!_formKey.currentState!.validate()) return;

            context.read<LoginBloc>().add(LoginFormSubmitEvent());
          },
          child: state.status == LoginStatus.authentication
              ? Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SizedBox(
                      height: 20,
                      width: 20,
                      child: CircularProgressIndicator(
                        color: Colors.white,
                        strokeWidth: 1.5,
                      ),
                    ),
                    SizedBox(width: 10),
                    Text('Sign In'),
                  ],
                )
              : Text(
                  'Sign In',
                ),
          style: ElevatedButton.styleFrom(
            primary: AppColors.green,
            textStyle: TextStyle(fontSize: 16),
            shape: new RoundedRectangleBorder(
              borderRadius: new BorderRadius.circular(30.0),
            ),
            elevation: 4,
          ),
        ),
      );
}
