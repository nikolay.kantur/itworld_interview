import 'package:itworld_interview/login/data/login_provider.dart';

class LoginRepository {
  final LoginProvider loginProvider;
  LoginRepository({required this.loginProvider});

  Future<String> signIn(
      {required String login, required String password}) async {
    final Map<String, dynamic> respRaw =
        await loginProvider.signIn(login: login, password: password);
    String accessToken = respRaw['accessToken'];
    return accessToken;
  }
}
