import 'dart:convert';

import 'package:http/http.dart' as http;

import 'package:itworld_interview/shared/provider_errors.dart';

class LoginProvider {
  final String baseUrl = 'http://mobile-test.itfox-web.com:80';

  Future<Map<String, dynamic>> signIn({
    required String login,
    required String password,
  }) async {
    final Uri uri = Uri.parse('$baseUrl/public/testAuth');
    final http.Request req = http.Request('POST', uri)
      ..body = '{"login":"$login","password":"$password"}'
      ..headers.addAll({
        "Content-type": "application/json",
      });

    final http.StreamedResponse response =
        await req.send().timeout(const Duration(seconds: 5));

    switch (response.statusCode) {
      case 200:
        final respStr = await response.stream.bytesToString();
        return jsonDecode(respStr);
      case 401:
        throw WrongCredentials();
      default:
        throw UnexpectedResponce();
    }
  }
}
